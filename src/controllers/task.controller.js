const pool = require("../db");

const getAllTask = async (req, res) => {
  try {
    const alltask = await pool.query("SELECT * FROM accounts");
    res.json(alltask.rows);
  } catch (error) {
    //next(error)
    console.log(error.message);
    res.json({ error: error.message });
  }
};

const getTask = async (req, res, next) => {
  const { user_id } = req.params;
  try {
    const task = await pool.query("SELECT * FROM accounts WHERE user_id = $1", [
      user_id,
    ]);

    if (task.rows.length === 0) {
      return res.status(404).json({
        message: "Not found",
      });
    }
    res.json(task.rows[0]);
  } catch (error) {
    //next(error);
    console.log(error.message);
    res.json({ error: error.message });
  }
};

const createTask = async (req, res, next) => {
  const { username, password } = req.body;

  try {
    const result = await pool.query(
      "INSERT INTO accounts (username, password) VALUES ($1, $2) RETURNING *",
      [username, password]
    );

    res.json(result.rows[0]);
    console.log(result);
  } catch (error) {
    //next(error);
    console.log(error.message);
    res.json({ error: error.message });
  }
};

const deleteTask = async (req, res) => {
  const { user_id } = req.params;
  try {
    const task = await pool.query(
      "DELETE FROM accounts WHERE user_id = $1 RETURNING *",
      [user_id]
    );

    if (task.rowCount === 0) {
      return res.status(404).json({
        message: "Not found",
      });
    }
    return res.sendStatus(204);
  } catch (error) {
    //next(error);
    console.log(error.message);
    res.json({ error: error.message });
  }
};

const updateTask = async (req, res) => {
  const { user_id } = req.params;
  const { username, password } = req.body;

  try {
    const taskupdate = await pool.query(
      "UPDATE accounts SET username = $1, password = $2 WHERE user_id = $3 RETURNING *",
      [username, password, user_id]
    );

    if (taskupdate.rows.length === 0) {
      return res.status(404).json({
        message: "Not found",
      });
    }

    return res.json(taskupdate.rows[0]);
  } catch (error) {
    //next(error);
    console.log(error.message);
    res.json({ error: error.message });
  }
};

module.exports = {
  getAllTask,
  getTask,
  createTask,
  deleteTask,
  updateTask,
};

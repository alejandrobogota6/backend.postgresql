const { Router } = require("express");
//const pool = require('../db');
const {
  getAllTask,
  getTask,
  createTask,
  deleteTask,
  updateTask,
} = require("../controllers/task.controller");

const router = Router();

router.get("/tasks", getAllTask);

router.get("/tasks/:user_id", getTask);

router.post("/tasks", createTask);

router.delete("/tasks/:user_id", deleteTask);

router.put("/tasks/:user_id", updateTask);

module.exports = router;

//router.get('/tasks', async(req, res) => {
//res.send('Traer Usuarios');
//const result = await pool.query('SELECT NOW()')
//console.log(result)
//res.json(result.rows[0].now)
//})


// router.get('/tasks/10', (req, res) => {
//     res.send('Un solo usuario');
// })


// router.post('/tasks', (req, res) => {
//     res.send('Creando un usuario');
// })

// router.delete('/tasks', (req, res) => {
//     res.send('Eliminando un usuario');
// })

// router.put('/tasks', (req, res) => {
//     res.send('Actualizar usuario');
// })




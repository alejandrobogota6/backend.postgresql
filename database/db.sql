CREATE DATABASE root

CREATE TABLE accounts (
	user_id serial PRIMARY KEY,
	username VARCHAR ( 255 ) UNIQUE NOT NULL,
	password VARCHAR ( 50 ) NOT NULL
);